# -*- coding: utf-8 -*-
import re
import urllib.request

from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

from config import *

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def _crawl_youtube_chart(text):
    if not "go" in text:
        return "`@<봇이름> go` 와 같이 멘션해주세요?."

    # 여기에 함수를 구현해봅시다.
    url = "https://www.youtube.com/feed/trending?gl=KR&hl=ko"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    # 웹 클래스와 soup로 긁어온 이름이 다를수있으니 혹시나 오류나면 soup를 프린트로 찍어본다.
    #print(soup)

    url = "https://www.youtube.com"
    titleBlocks = []
    imageBlocks=[]
    linkBlocks=[]
    charts = []
    count = 0
    count2 = 0

    mainTitle = SectionBlock(text="*★ 유튜브 인기 순위 ★*")
    division = SectionBlock(text= "*-----------------------------------------------------------------------------------------------------*")
    charts.append(mainTitle)

    for data in (soup.find_all("div", class_="yt-lockup-content"))[:10]:
        count2 +=1
        title = data.find("a", class_="yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink spf-link")["title"]
        textBlock = SectionBlock( text = "*" + str(count2)+"위 : " + title + " * ")
        titleBlocks.append(textBlock)

        location = data.find("a", class_="yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink spf-link")["href"]
        linkBlock = SectionBlock(text = "*" + url + location + "*")
        linkBlocks.append(linkBlock)

    for data in (soup.find_all("span", class_="yt-thumb-simple"))[:10]:
        #print(data)
        #print(44)

        count +=1
        if count < 7 :
            image=  data.find("img")["src"]
        else :
            image = data.find("img")["data-thumb"]

        image_block = ImageBlock(
            image_url=image,
            alt_text="이미지 없음" )
        imageBlocks.append(image_block)

    for i in range(0, 10):
        charts.append(imageBlocks[i])
        charts.append(titleBlocks[i])
        charts.append(linkBlocks[i])
        charts.append(division)


    #print(charts)

    return charts

    #return '\n'.join(thumbnails)

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    message = _crawl_youtube_chart(text)

    slack_web_client.chat_postMessage(
        channel=channel,
        #text=message,
        blocks=extract_json(message)
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5002)
